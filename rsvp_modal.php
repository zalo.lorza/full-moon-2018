<!-- RSVP Modal -->
        
<div class="rsvp_modal modal">
            <?php 
            /*
            <div class="modal_transparent_layer modal_close"></div>
            */
            ?>
            <div class="modal_content">
                <div class="rsvp_container">
                
                    <h1 class="form_title">RSVP For The Chance To Win:</h1>
                
                    <form class="rsvp_form" action="<?=$env['relative_path']?>/includes/mailchimp_form_submit.php">
                        <div class="form_field">
                            <label>First name:</label>
                            <input type="text" name="fname" placeholder="" required>
                        </div>
                        <div class="form_field">
                            <label>Last name:</label>
                            <input type="text" name="lname" placeholder="" required>
                        </div>
                        <div class="form_field">
                            <label>Email:</label>
                            <input type="email" name="email" placeholder="" required>
                        </div>
                        <div class="form_field">
                            <label>Phone:</label>
                            <input type="phone" name="phone" placeholder="">
                        </div>
                        <input type="hidden" name="listID" value="2cc5758439">
                        <input type="hidden" name="ad" value="false">
                        <div class="form_submit_container">
                            <button type="submit">
                                <img src="<?=$env['relative_path']?>/img/enter_button.png" />
                            </button>
                        </div>
                    </form>
                
                    <div class="rsvp_message"></div>
                
                </div>
            </div>
            <span class="close_icon modal_close"></span>
        </div>