
<?php if($env['layout']['rsvp_modal']) include($env['relative_path'].'/rsvp_modal.php'); ?>

        <script src="<?=$env['relative_path']?>/scripts/jquery-3.3.1.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/velocity-animate@2.0/velocity.min.js"></script>
        <script src="https://cdn.polyfill.io/v2/polyfill.js?features=default,fetch,Array.prototype.find,Array.prototype.findIndex,Array.prototype.includes,Object.entries,Element.prototype.closest"></script>
        <script src="https://cdn.plyr.io/3.2.0/plyr.js"></script>
        <script src="<?=$env['relative_path']?>/scripts/plugin_scripts.min.js?<?=SCRIPTS_VERSION?>"></script>
        <script src="<?=$env['relative_path']?>/scripts/custom_scripts.min.js?<?=SCRIPTS_VERSION?>"></script>

        </body>
</html>