(function($) {

	$(document).ready(function() {

	"use strict"; 

		/*======================== 
		MOBILE NAVIGATION
		========================*/

		// Open the fullscreen overlay

		$('.menu_icon').click(function(){
			if($(this).hasClass('navigation_open')) {
				$('.mobile_navigation').fadeOut('fast');
				$(this).removeClass('navigation_open');
			}
			else {
				$('.mobile_navigation').fadeIn('fast');
				$(this).addClass('navigation_open');
			}
		});

		// Clicking a navigation link within the overlay scrolls to the section of the page and closes it

		$('.mobile_navigation_list li a').click(function(){
			$('.mobile_navigation').fadeOut('fast');
			$('.menu_icon').removeClass('navigation_open');
		});

		/*======================== 
		Smooth Scroll Initialization
		========================*/

		// Desktop nav

		$('.main_navigation li a').smoothScroll({
			speed: 800,
			offset: -20
		});

		// Adjust offset for mobile to account for the fixed header

		$('.mobile_navigation_list li a').smoothScroll({
			offset: 0
		});

		/*======================== 
		Navigation Indicators
		========================*/

		// Save all sections that can be scrolled to

		const sections = document.querySelectorAll('.fm_section');

		// Check which section we are currently on and update the corresponding navigation item

		function checkSection() {
			Array.from(sections).forEach(section => {
				
				// Calculate the positioning of the section

				const height = section.offsetHeight;
				const topPosition = section.offsetTop;
				const bottomPosition = topPosition + height;

				// Find the sections corresponding link

				const sectionName = section.getAttribute('id');
				const sectionNavLink = document.querySelector('.main_navigation a[href="#' + sectionName + '"');

				// If scroll position is within current section then indicate it in the navigation

				const scrollBelowSectionTop = window.scrollY >= topPosition - 100;
				const scrollAboveSectionBottom = window.scrollY < bottomPosition - 100;

				if(scrollBelowSectionTop && scrollAboveSectionBottom) {
					sectionNavLink.classList.add('active');
				}
				else {
					sectionNavLink.classList.remove('active');
				}

			});
		};

		// On scroll update the section

		window.addEventListener('scroll', debounce(checkSection, 50));

	});

})(jQuery);