(function ($) {

    $(document).ready(function () {

        "use strict";

        /*-----------
        Eventbrite Embed
        ---
        If a user is using Safari less than v11 then the Eventbrite embedded checkout doesn't work.
        -----------*/
        
        // Elements
        
        const eventbriteEmbed = document.querySelector('.tickets_container .eventbrite_embed_container');
        const eventbriteFallback = document.querySelector('.tickets_container .eventbrite_fallback_container');
        
        if(eventbriteEmbed) {
            if (bowser.name === 'Safari' && Number(bowser.version) < 11) {
                eventbriteEmbed.style.display = 'none';
                eventbriteFallback.style.display = 'block';
            }
            else {
                eventbriteEmbed.style.display = 'block';
                eventbriteFallback.style.display = 'none';
            }
        }

    });

})(jQuery);