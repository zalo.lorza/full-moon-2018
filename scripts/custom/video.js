(function($) {

	$(document).ready(function() {

    "use strict";

        /*================================= 
		Play Video On Scroll
		=================================*/
        
        // Elements
        
        const locationSection = document.querySelector('.location_section');

        if (locationSection) {
            
            const video = locationSection.querySelector('video');
            
            window.addEventListener('scroll', debounce(() => {
                if(elementInView(video)) {
                    video.play();
                }
                else {
                    video.pause();
                }
            }), 20);

        }
        
    });
    
})(jQuery);