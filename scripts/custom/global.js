/*================================= 
DEBOUCE
=================================*/

// Handle taxing js tasks
// https://davidwalsh.name/javascript-debounce-function

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this, args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

/*================================= 
MEDIA QUERIES
=================================*/

// Interactions just for desktop

const mq = window.matchMedia("(min-width: 1024px)");

/*================================= 
Element In View
---
Mostly used during window scroll event but can be used in other use cases.
Element should be positioned relative. Will add flexibility for absolute position later.
=================================*/

function elementInView(element) {
  
  const viewportOffset = element.getBoundingClientRect();

  // Element Measurements

  const elementTop = viewportOffset.top;
  const elementHeight = element.offsetHeight;
  const elementMiddle = elementTop + (elementHeight / 2);
  const elementBottom = elementTop + elementHeight;

  // Window Measurements

  let windowPosition = window.scrollY;
  let windowHeight = window.innerHeight;
  let windowBottomPosition = windowPosition + windowHeight;
  
  // Check if element is in view
  
  let elementInView = (elementMiddle <= windowHeight) && (elementBottom >= 0);

  if (elementInView) {
    return true;
  }
  else {
    return false;
  }

}